function isValidEmail(email) {
    var pattern = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
    return new RegExp(pattern).test(email);

}
function validateObjectId(objectId) {
    if (typeof (objectId) != 'string' || !(objectId) || !(objectId.match(/^[0-9a-fA-F]{24}$/)))
        return false;
    else
        return true;
    // return ( objectId.match( /^[0-9a-fA-F]{24}$/ ) );
}


module.exports = {
    isValidEmail,validateObjectId
}