
const Board = require('../modal/boardModel');
const createBoard = async (req,res,next)=>{
    console.log(req.user);
  let board = new Board({
        name : req.body.name,
        color : req.body.color,
        userId : req.user._id
    })
    board.save().then(resp =>{
        res.json(resp)
    }).catch(err =>{
        res.json(err)
    })
}

const deleteBoard = async (req,res,next)=>{
    console.log(req.user);
  let params = {
       _id : req.body.boardId
    }
    Board.deleteOne(params).then(resp =>{
      
       res.status(200).send({result : "board delete sucessfully"})
    }).catch(err =>{
        res.json(err)
    })
}

const list = (req,res,next)=>{
    let user = {}
    if(req.query.userId){
        user.userId = req.query.userId
    }
    else{
        user.userId = req.user._id
    }
  
    Board.find(user).then(resp =>{
        res.json(resp)
    }).catch(err =>{
        res.json(err)
    })
}

module.exports = {
    createBoard,list,deleteBoard
}