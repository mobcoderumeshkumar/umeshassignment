
const Task = require('../modal/taskModel')

const createTask = (req,res)=>{
  
    let task = new Task({
        name : req.body.name,
        status : req.body.status,
        boardId : req.body.boardId,

    })
    task.save().then(resp =>{
        console.log(resp);
        res.json(resp)
    }).catch(err =>{
        
        res.json(err)
    })
}

const updateStaus = (req,res)=>{
   console.log(req.body.taskId);
    let task = new Task({
        _id : req.body.taskId,
       
    })
    console.log(task);
    Task.findByIdAndUpdate({_id : task._id},{$set:{status : req.body.status}}).then(resp =>{
        console.log(resp);
        res.json(resp)
    }).catch(err =>{
        
        res.json(err)
    })
}
const list = (req,res)=>{
   let data ={}
     data.boardId = req.query.boardId
     if(req.query.status){
         data.status =req.query.status 
     }
   Task.find({boardId : data.boardId}).then(resp =>{
        res.json(resp)
    }).catch(err =>{
        res.json(err)
    })
}

const deleteTask = (req ,res ) =>{
    let taskId = req.body.taskId ;
    Task.remove({_id : taskId})
    .then(resp =>{
        res.status(200).send({result : "task delete sucessfully"})
    }).catch(err =>{
        res.json(err)
    })
}

module.exports = {
    createTask,list,updateStaus,deleteTask
}



