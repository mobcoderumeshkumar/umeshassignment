const redisClient = require('../redis');
const jwtdecode  = require('jwt-decode')


const verifyUserToken = async(req,res,next) =>{
    let acessToken = req.headers.token;
    const token = await redisClient.get('user', (err, reply) => {
        if (err) throw err;
        });
        if (token != acessToken) {
           res.status(200).send({ result : 'token is expired'});
        }

  const userObj =  jwtdecode(token);
  req.user = userObj.username;  
  next();
   }

   module.exports = {
       verifyUserToken
   }
  