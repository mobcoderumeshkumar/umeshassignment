const appUtils = require('../appUtils')
const createBoard = (req,res,next)=>{
    let {name, color } = req.body;
    
    if(!name){
        res.status(200).send({result : 'name cannot be empty', });
    }
    if(!color){
        res.status(200).send({result : 'color cannot be empty', });
    }
  
    next()
}

module.exports = {
    createBoard
}