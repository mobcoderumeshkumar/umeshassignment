const appUtils = require('../appUtils')
const createTask = (req,res,next)=>{
    let {name, status,boardId } = req.body;
    
    if(!name){
        res.status(200).send({result : 'name cannot be empty', });
    }
    if(!status){
        res.status(200).send({result : 'status cannot be empty', });
    }
    if(!boardId){
        res.status(200).send({result : 'boardId cannot be empty', });
    }else if(!appUtils.validateObjectId(boardId)){
        res.status(200).send({result : 'please enter valid id', });
    }
    next()
}

module.exports = {
    createTask
}