const appUtils = require('../appUtils')
const userSignup = (req,res,next)=>{
    let {email , password} = req.body;
    
    if(!email){
        res.status(200).send({result : 'Email cannot be empty', });
    }else if(!appUtils.isValidEmail(email)){
        res.status(200).send({result : 'Please input a valid email' })
    }
    if(!password){
        res.status(200).send({result : 'password cannot be empty', });
    }
    next()
}

module.exports = {
    userSignup
}