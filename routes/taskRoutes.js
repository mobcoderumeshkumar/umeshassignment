const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController')
const middleware = require('../middleware/verifyToken')
const validator = require('../validator/taskVaditaor')

router.post('/create',[middleware.verifyUserToken,validator.createTask],taskController.createTask);
router.put('/update',[middleware.verifyUserToken],taskController.updateStaus);
router.get('/list',[middleware.verifyUserToken],taskController.list);
router.delete('/deleteTask',[middleware.verifyUserToken],taskController.deleteTask)

module.exports = router;