const express = require('express');
const router = express.Router();
const userContoller = require('../controllers/userControllers');
const validator = require('../validator/userValidator')
const middleware = require('../middleware/verifyToken')
router.use(express.json());

router.post('/signUp',[validator.userSignup],userContoller.signUp);
router.post('/login',[validator.userSignup],userContoller.login);
router.post('/logout',[middleware.verifyUserToken],userContoller.logout);

module.exports = router;


//mongorestore --db lgnode /home/Downloads/lg_dev