const express = require('express');
const { verify } = require('../controllers/authenticate');
const router = express.Router();
const boardController = require('../controllers/boardController')
const middleware = require('../middleware/verifyToken')
const validator = require('../validator/boardValidator')

router.post('/create',[middleware.verifyUserToken,validator.createBoard],boardController.createBoard);
router.get('/list',[middleware.verifyUserToken],boardController.list);
router.delete('/delete',[middleware.verifyUserToken],boardController.deleteBoard);


module.exports = router;