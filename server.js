const express = require('express');
const mongoose = require('mongoose');
const morgen   = require('morgan');
const bodyParser = require('body-parser')
const userRoute  = require('./routes/userRoutes');
const boardRoute  = require('./routes/boardRoutes');
const taskRoute  = require('./routes/taskRoutes');
const cors = require('cors');
const app = express();
app.use(cors());

require('./redis')
require('./config')
var url = 'mongodb+srv://umesh:umesh12345@test1.ujgio.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

mongoose.connect(url, { useNewUrlParser: true }, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});
app.use(morgen('dev'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
const Port = process.env.PORT || 3500
app.listen(Port,()=>{
    console.log(`sever started at ${Port}!!!`)
})

app.use('/api/user',userRoute);
app.use('/api/board',boardRoute);
app.use('/api/task',taskRoute);

