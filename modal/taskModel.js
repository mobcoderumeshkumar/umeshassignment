var mongoose = require("mongoose");
var Schema = mongoose.Schema;


var TaskSchema = new Schema({

    name: {
        type: String,
        required: true
      },
    boardId:
      {
          type: mongoose.Schema.Types.ObjectId,// for coach details
         // ref: constants.DB_MODEL_REF.COACH
      },
   status :{
       type : Number, //All tasks 1 for  ‘To do’,2 for Progress,3 for on hold,4 for Complete ,5 for Relaesd ,6 for 
        required: true
     },
   isCreated : {
        type: Date,
        default: Date.now  
    } ,
  isDel: {
        type: Boolean,
        default: false
    },
   
   
});



//Export Task module
const task=mongoose.model("task",TaskSchema);
 module.exports = task;




