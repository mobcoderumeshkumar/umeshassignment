var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var BoardSchema = new Schema({

    name: {
        type: String,
        required: true
      },
      userId:
      {
          type: mongoose.Schema.Types.ObjectId,// for coach details
         // ref: constants.DB_MODEL_REF.COACH
      },
   color :{
       type : String,
       required: true
     },
   isCreated : {
        type: Date,
        default: Date.now  
    } ,
   isDel: {
        type: Boolean,
        default: false
    },
   
   
});



//Export Board module
const board=mongoose.model("board",BoardSchema);
 module.exports = board;


